import React, { PureComponent } from 'react';
import $ from 'jquery';

class Photo extends PureComponent {
    constructor(props) {
        super(props);
        this.onClickHandler = this.onClickHandler.bind(this)
    }

    onClickHandler = (e) => {
        $('#galleryImage').attr("src", e.target.src);
    }

    render() {
        return (
            <div className="p-md-2 col-sm-4" style={{ width: "30%" }}>
                <img
                    className='img-thumbnail img-fluid'
                    src={this.props.source}
                    alt="Loading Failed"
                    data-toggle="modal"
                    data-target="#myModal"
                    onClick={(e) => this.onClickHandler(e)} />


                <div className="modal fade" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="modal-dialog col-9" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">
                                        <img className='gallery-thumbnail' id="galleryImage" alt="Not available" />
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Photo;