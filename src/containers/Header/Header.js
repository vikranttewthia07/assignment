import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';
import $ from 'jquery';
import './Header.css';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        }
        this.inputRef = React.createRef();
    }

    componentDidMount() {
        console.log('Header')
        let list = [];
        if(JSON.parse(localStorage.getItem('list'))){
         list = JSON.parse(localStorage.getItem('list'));
        }
        if (list.length > 0) {
            this.setState({ list: list })
        }
    }

    onChangeHandler = (e) => {
        if (e.target.value.length === 0) {
            this.props.onFetchPhotos();
        }
        else {
            this.props.onSearchPhotos(e.target.value);
        }
    }

    //Suggestion box search
    onClickHandler = (e) => {
        this.props.onSearchPhotos(e.target.value);;
        $('#my-input-box').val(e.target.value)
    }

    clearList = () => {
        localStorage.removeItem('list');
        this.setState({ list: [] })
        $('.options').hide()
    }

    showSuggestions = () => {
        if (this.state.list.length > 0) {
            $('.options').show()
        }
    }

    //Clicking on button
    searchPhotos = (e) => {
        this.storeSuggestions($('#my-input-box').val());
        $('.options').hide()
    }

    keyPress = (e) => {
        if (e.keyCode === 13) {
            this.storeSuggestions(e.target.value)
            $('.options').hide()
        }
    }

    storeSuggestions = (value) => {
        let newList = [...this.state.list, value]
        this.setState({
            list: newList
        })

        localStorage.setItem('list', JSON.stringify(newList))
    }

    render() {
        return (

            <div className="fixed-top header w-100 pt-md-4 pb-md-3">
                <h3 className="text-white">Search Photos</h3>

                <form className="d-flex flex-md-row justify-content-center">
                    <div className="form-group d-flex flex-md-column align-items-center w-25 w-sm-50 position-relative">
                        <input
                            id="my-input-box"
                            ref={this.inputRef}
                            className="form-control input-box w-100"
                            onChange={(e) => this.onChangeHandler(e)}
                            onSelect={() => this.showSuggestions()}
                            onKeyDown={(e) => this.keyPress(e)}
                            type="text"
                            placeholder="Search here"
                            />
                        <i className="fas fa-search position-absolute" style={{
                            right: "10px",
                            top: "6px",
                            color: '#24827d',
                            cursor: "pointer"
                        }}
                            onClick={(e) => this.searchPhotos(e)}></i>
                        <div className="position-absolute form-group w-100 bg-white options">
                            {
                                JSON.parse(localStorage.getItem('list')) ?
                                    JSON.parse(localStorage.getItem('list')).reverse().map((option, index) => (
                                        <option className="w-25" onClick={(e) => this.onClickHandler(e)} key={index} value={option}>{option}</option>
                                    )) : null
                            }
                            <button type="button" className="position-absolute btn btn-danger" onClick={() => this.clearList()}>Clear All</button>
                        </div>
                    </div>
                </form>
            </div >
        )
    }
}

const mapStateToProps = state => {
    return {
        photos: state.photos
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSearchPhotos: (tag) => dispatch(actions.searchPhotos(tag)),
        onFetchPhotos: () => dispatch(actions.fetchPhotos())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);