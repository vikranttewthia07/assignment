import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions'
import Photo from '../../components/Photo/Photo';
import Loader from '../../components/Loader/Loader';
import InfiniteScroll from 'react-infinite-scroll-component';
import $ from 'jquery';

class Main extends PureComponent {
    constructor(props) {
        super(props);
        this.listRef = React.createRef();
    }

    componentDidMount() {
        this.props.onFetchPhotos();
    }


    render() {
        $('.infinite-scroll-component__outerdiv').css("width", "100%");
        return (
            <div id="list-container"
                ref={this.listRef}
                className="row"
            >
                <InfiniteScroll
                    dataLength={this.props.photos ? this.props.photos.length : 0}
                    next={this.props.onFetchMorePhotos()}
                    hasMore={true}
                    loader={<Loader />}
                    className="d-flex w-100 pr-md-2 justify-content-md-around flex-wrap"
                    style={{ marginTop: "8rem" }}
                >

                    {this.props.photos ?
                        (this.props.photos.map((photo, i) => (
                            <Photo
                                key={i}
                                source={`https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}_m.jpg`}
                            />
                        ))) : null}

                </InfiniteScroll>
            </div>
        )
    }
};

const mapStateToProps = state => {
    return {
        photos: state.photos,
        loading: state.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchPhotos: () => dispatch(actions.fetchPhotos()),
        onFetchMorePhotos: () => dispatch(actions.fetchMorePhotos())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);
