import * as actionTypes from './actionTypes';
import axios from 'axios';

const params = {
    api_key: "7d82cabb7b1bd711b71f60d3d7de4624",
    format: 'json',
    method: `flickr.photos.getRecent`,
    nojsoncallback: 1,
    per_page: 100
}

export const fetchPhotosStart = () => {
    return {
        type: actionTypes.FETCH_PHOTOS_START
    };
};

export const fetchPhotosSuccess = (photos) => {
    return {
        type: actionTypes.FETCH_PHOTOS_SUCCESS,
        photos
    }
}

export const fetchPhotosFail = (error) => {
    return {
        type: actionTypes.FETCH_PHOTOS_SUCCESS,
        error
    }
}

export const fetchMorePhotosStart = () => {
    return {
        type: actionTypes.FETCH_MORE_PHOTOS_START
    };
};

export const fetchMorePhotosSuccess = (photos) => {
    return {
        type: actionTypes.FETCH_MORE_PHOTOS_SUCCESS,
        photos
    }
}

export const fetchMorePhotosFail = (error) => {
    return {
        type: actionTypes.FETCH_MORE_PHOTOS_SUCCESS,
        error
    }
}

export const searchPhotosStart = () => {
    return {
        type: actionTypes.SEARCH_PHOTOS_START
    };
};

export const searchPhotosSuccess = (photos) => {
    return {
        type: actionTypes.SEARCH_PHOTOS_SUCCESS,
        photos
    }
}

export const searchPhotosFail = (error) => {
    return {
        type: actionTypes.SEARCH_PHOTOS_SUCCESS,
        error
    }
}

export const fetchPhotos = () => {
    return dispatch => {
        console.log(params)
        dispatch(fetchPhotosStart());
        axios.get('https://api.flickr.com/services/rest/', { params })
            .then(({ data }) => {
                dispatch(fetchPhotosSuccess(data.photos.photo));
            })
            .catch(err => {
                dispatch(fetchPhotosFail(err));
            })
    };
}

export const fetchMorePhotos = () => {
    return dispatch => {
        dispatch(fetchMorePhotosStart());
        axios.get('https://api.flickr.com/services/rest/', { params })
            .then(({ data }) => {
                dispatch(fetchMorePhotosSuccess(data.photos.photo));
            })
            .catch(err => {
                dispatch(fetchMorePhotosFail(err));
            })
    };
}

export const searchPhotos = (text) => {
    return dispatch => {
        dispatch(searchPhotosStart());
        let params = {
            api_key: "7d82cabb7b1bd711b71f60d3d7de4624",
            format: 'json',
            method: `flickr.photos.search`,
            nojsoncallback: 1,
            per_page: 100,
            tags: `${text}`,
            safe_search: 1
        }

        axios.get('https://api.flickr.com/services/rest/', { params })
            .then(({ data }) => {
                dispatch(searchPhotosSuccess(data.photos.photo));
            })
            .catch(error => {
                dispatch(searchPhotosFail(error));
            })

    }
}